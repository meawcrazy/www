
    <div class="carousel slide" id="featured" data-ride="carousel">


        <ol class="carousel-indicators">
            <li data-target="#featured" data-slide-to="0" class="active"></li>
            <li data-target="#featured" data-slide-to="1"></li>
            <li data-target="#featured" data-slide-to="2"></li>
            <li data-target="#featured" data-slide-to="3"></li>
        </ol>


        <div class="carousel-inner">
            <div class="item active">
                <img src="images/c1.jpg" alt="Chania">
            </div>
            <div class="item">
                <img src="images/c2.jpg" alt="Flower">
            </div>
            <div class="item">
                <img src="images/c3.jpg" alt="Flower">
            </div>
            <div class="item">
                <img src="images/c4.jpg" alt="Flower">
            </div>
        </div>

        <a class="left carousel-control" href="#featured" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#featured" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>

    </div>
