<!DOCTYPE html>
<html lang="en">

	<head> 
		<meta charset100="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Machine Work</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
		<style>


		</style>

	</head>


	<body>
		<!--<?php include 'carousel.php'; ?>-->
		<?php include 'header.php'; ?>
		<h2>&nbsp;&nbsp;Order</h2>

		<div class=" container">

			<div class="row">
				<div class="col-xs-6">
					Order ID<br>
					Order Date<br>
					Due Date<br>
				</div>
				<div class="col-xs-6">
					<button id="reset" class="btn btn-danger pull-right ">Delete this order</button>


				</div>

			</div>
			
			<hr class="colorgraph">

			<div class="row">
				<!--sku-->
				<span id="sku"></span>
				<!--sku end-->

			</div> <!--row-->

			<table class="table-borderless" id="footer">
				<th width="150">
					Total Amount:
				</th>
				<th id="totalamt" class="pull-right">
					200,000
				</th>

			</table>

			<br>

			<button id="reset" class="btn btn-danger pull-left">Reset all quantity to 0</button>
			<button id='confirm' class="pull-right btn btn-primary btn ">Confirm order</button>

			<br> <br>  <br>


		</div> <!--container-->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

		<script src="js/order.js"></script> 


	</body>

</html>
