<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Machine Work</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
		<style>

		</style>

	</head>

 
	<body>
		<?php include 'carousel.php'; ?>
		<?php include 'header.php'; ?>
		<h2>&nbsp;&nbsp;Member Profile</h2>

		<div class="container">

			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label class="control-label col-sm-2" for="email">Email:</label>
					<div class="col-sm-10">
						<input type="email" class="form-control" id="email" placeholder="Enter email">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Password:</label>
					<div class="col-sm-10"> 
						<input type="password" class="form-control" id="pwd" placeholder="Enter password">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="Name">Name:</label>
					<div class="col-sm-10"> 
						<input type="text" class="form-control" id="name" placeholder="Enter name">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="surnameame">Surname:</label>
					<div class="col-sm-10"> 
						<input type="text" class="form-control" id="surname" placeholder="Enter surname">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="addresship">Ship to:</label>
					<div class="col-sm-10"> 
						<input type="text" class="form-control" id="addressship" placeholder="Enter address">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="addressbill">Bill to:</label>
					<div class="col-sm-10"> 
						<input type="text" class="form-control" id="addressbill" placeholder="Enter address">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="pwd">Telephone:</label>
					<div class="col-sm-10"> 
						<input type="text" class="form-control" id="telephone" placeholder="Enter phone number">
					</div>
				</div>
			</form>

			<button id='confirm' class="pull-right btn btn-primary btn ">Register</button>



		</div> <!--container-->


		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>

</html>