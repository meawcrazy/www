/*jslint white: true, sloppy: false, vars: false, indent: 0*/
$(document).ready(function () {
    
    //$("body").append('<p>just added</p>');
    /*fill sku function*/
    var skus = [['A', 2000, 100],
                ['B', 500, 20],
                ['C', 20, 50],
                ['D', 100, 0],
                ['E', 500, 40],
                ['F', 4, 130]                
               ];

    var item = '';
    for (i=0;i<skus.length;i++) {
        item=
            '<div class="col-sm-12 col-md-6 col-lg-4"  >'+
            '<div class="panel panel-default">'+
            '<div class="panel-heading">'+
            skus[i][0] + 
            '</div>'+
            '<div class="panel-body">'+

            '<table class="table-borderless" id="tab_logic">'+
            '<tbody>'+
            '<tr>'+
            '<th width="80">'+
            'Price:'+
            '</th>'+
            '<td id="sku_price_a" class="pull-right">'+
            skus[i][1] +
            '</td>'+
            '</tr>'+
            '<tr>'+
            '<th>'+
            'Quantity:'+
            '</th>'+
            '<td class="pull-right">'+
            '<input id="qty_'+
            skus[i][0].toLowerCase()+
            '" type="text" size="3" class="text-right" value="'+
            skus[i][2] + 
            '">'+
            '</td>'+
            '</tr>'+
            '</tbody>'+
            '</table>'+
            '<br>'+
            '<img src="images/_item_a.jpg" role="button" class="img-rounded img-responsive center-block"/>'+
            '</div>'+
            '</div>'+
            '</div>';

        $("#sku").append(item);
    }



    /*bottom line function*/
    $("#reset").click(function(){
        var qty='';
        for(i=0;i<skus.length;i++)
        {
            qty = "#qty_"+skus[i][0].toLowerCase();

            $(qty).val("0")        
        }

    });

    $("#confirm").click(function(){

    });


    /*qty calculating function*/
    $("#qty_a").change(function(){
        totalAmt();

    });

    var totalamt=0;
    function totalAmt() {
        totalamt = qty_a * skus[0][1];
    };


}); /*Very impartant*/