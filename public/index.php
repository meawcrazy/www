<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Machine Work</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
	</head>

	<body>
		<?php include 'carousel.php'; ?>
		<?php include 'header.php'; ?>
		<h2>&nbsp;&nbsp;Production Planning and Control</h2>

		<div class="content container">

			<h3>Automation Engineering</h3>
			<h5>Faculty of Engineering<br>
				King Mongkut’s Institute of Technology Ladkrabang</h5>



			<p>Production Planing Control</p>
			<ul>
				<li>Master Production Schedule (MPS)</li>
				<li>Material Requirements Planning (MRP)</li>
				<li>Bill of Materials (BOM)</li>
				<li>Scheduling</li>
				<li>Advanced Planning and</li>
				<li>Scheduling (APS)</li>

			</ul>

			<br>

			<!--<button type="button" class="btn btn-primary">Enter site</button>-->

			<a href="signin.php" target="_self" class="btn btn-md btn-primary ">Enter site</a>


			<br><br><br><br>

		</div>

		<?php include 'footer.php'?>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>


	</body>

</html>