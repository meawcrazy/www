<!DOCTYPE html>
<html lang="en">

	<head> 
		<meta charset100="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Machine Work</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
		<style>

			input,
			select,
			textarea {
				max-width: 280px;
			}

		</style>

	</head>


	<body>
		<!--<?php include 'carousel.php'; ?>-->
		<?php include 'header.php'; ?>
		<h2>&nbsp;&nbsp;Batch create</h2>
		

		<div class="container-fluid">


			<form class="form-inline" role="form">
				<div class="form-group">
					<label for="batchno">Batch No:</label>

					<input type="text" class="form-control" id="batchno"  }>

				</div>
			</form>
			<br>

			<div class="dropdown">
				<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" id="method">Method <span class="caret"></span></button>
				<ul class="dropdown-menu">
					<li><a href="#">EDD</a></li>
					<li><a href="#">SPT</a></li>
					<li><a href="#">WSPT</a></li>
					<li class="divider"></li>
					<li><a href="#"><span class="glyphicon glyphicon-remove"></span> FCFS</a></li>
				</ul>
			</div>



			<div class="checkbox">
				<label><input type="checkbox"> Show this batch only and ready to Production</label>
			</div>
		</div>
		
		<div class="container">

			<div class="row">
				<table class="table table-bordered table-hover" id="tab_logic">
					<tbody>
						<tr>
							<th >Order No.</th>
							<th>Order date</th>
							<th>Due date</th>
							<th>Batch</th>
							<th class="text-center">Method</th>
							<th class="text-center">Select</th>

						</tr>
						<tr>
							<td>0001</td>
							<td>01/01/2515</td>
							<td>01/02/2515</td>
							<td>0001</td>
							<td class="text-center">EDD</td>
							<td class="text-center"><div class="checkbox">
								<label><input type="checkbox" value=""></label>
								</div>
							</td>
						</tr>
						<tr>
							<td>003</td>
							<td>01/02/2515</td>
							<td>01/03/2515</td>
							<td>0001</td>
							<td class="text-center">WSPT</td>
							<td class="text-center"><div class="checkbox">
								<label><input type="checkbox" value=""></label>
								</div>
							</td>
						</tr>
						<tr>
							<td>0006</td>
							<td>01/03/2515</td>
							<td>01/04/2515</td>
							<td>0001</td>
							<td class="text-center">EDD</td>
							<td class="text-center"><div class="checkbox">
								<label><input type="checkbox" value=""></label>
								</div>
							</td>
						</tr>


					</tbody>

				</table>

				<br>

				<button type="button" class="btn btn-danger">Reset</button>
				<button type="button" class="btn btn-primary">Batch</button>

				<button type="button" class="btn btn-success">Send batch to production</button>
			</div>
		</div> <!--container-->

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>

		<script type="text/javascript" >
			$(".dropdown-menu li a").click(function(){
				var selText = $(this).text();                
				$("#method").html(selText+' <span class="caret"></span>');
			});

		</script> 


	</body>

</html>
