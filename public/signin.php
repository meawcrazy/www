<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Machine Work</title>

        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet">
        <style>
 
         </style>

    </head>


    <body>
        <?php include 'carousel.php'; ?>
        <?php include 'header.php'; ?>
        <h2>&nbsp;&nbsp;Register | Sign in</h2>

        <div class="container">
            <div class="row" style="margin-top:20px">
                <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
                    <form name="form_login" method="post" action="signincheck.php" role="form">
                        <fieldset>
                            
                            
                            <div class="form-group">
                                <input name="email" type="text" id="email" class="form-control input-lg" placeholder="Email Address">
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" id="password" class="form-control input-lg" placeholder="Password">
                            </div>
                            <span class="button-checkbox">
                                <button type="button" class="btn" data-color="info">Remember Me</button>
                                <input type="checkbox" name="remember_me" id="remember_me" checked="checked" class="hidden">
                            </span>
                            <hr class="colorgraph">
                            <div class="row">                                
                                <div class="col-xs-6 col-sm-6 col-md-6"> <a href="register.php" target="_self" class="btn btn-lg btn-primary btn-block">Register</a> </div>
                                <div class="col-xs-6 col-sm-6 col-md-6">
                                    <input type="submit" name="Submit" value="Sign In" class="btn btn-lg btn-success btn-block">
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div> 
            </div>
        </div>



        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </body>

</html>