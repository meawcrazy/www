<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Machine Work</title>

		<link href="css/bootstrap.min.css" rel="stylesheet">
		<link href="css/custom.css" rel="stylesheet">
		<style>

		</style>

	</head>



	<body>
		<?php include 'carousel.php'; ?>
		<?php include 'header.php'; ?>



		<!--			<div class="row">

<div class="col-xs-12">
Name<br />
Company<br />
</div>

</div>
<hr class="colorgraph">-->

		<!--
<div class="row">
<a href="member.html" class="btn btn-danger" target="_blank">Delete</a>
</div>
-->
		<h2>&nbsp;&nbsp;Order history</h2>
		
		<div class="container-fluid">

			<button class="btn btn-primary" id="method"><span class="glyphicon glyphicon-plus"></span> New order</button>


			<div class="checkbox">
				<label><input type="checkbox"> Show this batch only and ready to Production</label>
			</div>
		</div>
		<div class="row">
			<div class="container">
				<table class = "table table-striped table-hover table-bordered">
					<!--<caption>Basic Table Layout</caption>-->

					<thead>
						<tr>
							<th>Order Number</th>
							<th>Order Date</th>
							<th class="text-right">Amount</th>
							<th >Status</th>

						</tr>
					</thead>

					<tbody>
						<tr>
							<td>0001</td>
							<td>01/01/2515</td>
							<td class="text-right">12,345</td>
							<td>Finished</td>

						</tr>

						<tr>
							<td>0005</td>
							<td>05/01/2515</td>
							<td class="text-right">343</td>
							<td>Production</td>
						</tr>
						<tr>
							<td>0005</td>
							<td>05/01/2515</td>
							<td class="text-right">343</td>
							<td>Pending</td>
						</tr>

					</tbody>

				</table>

			</div>
		</div>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>

</html>